import urllib.request
import urllib.parse
import json
import sys

class UninitializedBasket(Exception):
    pass

class AlreadyInitialized(Exception):
    pass

class LanaChallengeClient:
    def __init__(self, port):
        self.urlbase = f'http://localhost:{port}'
        self.basketId = None

    def newBasket(self):
        if self.basketId is None:
            response = urllib.request.urlopen(f'{self.urlbase}/newBasket').read()
            responseJson = json.loads(response)
            self.basketId = responseJson["basketId"]
            return responseJson["basketId"]
        else:
            raise AlreadyInitialized


    def addToBasket(self, productId):
        if self.basketId is not None:
            params = urllib.parse.urlencode({'basketId': self.basketId, 'productId': productId})
            urllib.request.urlopen(f'{self.urlbase}/addToBasket?{params}').read()
        else:
            raise UninitializedBasket

    def deleteBasket(self):
        if self.basketId is not None:
            params = urllib.parse.urlencode({'basketId': self.basketId})
            urllib.request.urlopen(f'{self.urlbase}/deleteBasket?{params}').read()
            self.basketId = None
        else:
            raise UninitializedBasket
    
    def totalPrice(self):
        if self.basketId is not None:
            params = urllib.parse.urlencode({'basketId': self.basketId})
            response = urllib.request.urlopen(f'{self.urlbase}/totalPrice?{params}').read()
            responseJson = json.loads(response)
            return responseJson["totalPrice"]
        else:
            raise UninitializedBasket

    def getBasket(self):
        if self.basketId is not None:
            params = urllib.parse.urlencode({'basketId': self.basketId})
            response = urllib.request.urlopen(f'{self.urlbase}/getBasket?{params}').read()
            return json.loads(response)
        else:
            raise UninitializedBasket

if __name__ == "__main__":
    port = int(sys.argv[0]) if len(sys.argv) > 1 else 8080
    lanaClient = LanaChallengeClient(port)
   
    selected_option = 0
    while selected_option != 6:
        
        print("Choose option: ")
        print("   1) New basket")
        print("   2) Add product to basket")
        print("   3) Get total price")
        print("   4) View current basket")
        print("   5) Delete basket")
        print("   6) Exit")
        selected_option = int(input())

        if selected_option >= 1 and selected_option <= 6:
            try:
                if selected_option == 1:
                    print("BasketId: " + lanaClient.newBasket())
                elif selected_option == 2:
                    product = int(input("Product (1:TSHIRT, 2:PEN, 3:MUG): "))
                    if product >= 1 and product <= 3:
                        lanaClient.addToBasket(product)
                    else:
                        print("Invalid product")
                elif selected_option == 3:
                    print("Total price: " + lanaClient.totalPrice())
                elif selected_option == 4:
                    basket = lanaClient.getBasket()
                    print("Basket: ")
                    print(basket)
                elif selected_option == 5:
                    lanaClient.deleteBasket()
            except UninitializedBasket:
                print("You need to create a basket first!")
            except AlreadyInitialized:
                print("You need to delete the current basket first")


        else:
            print("Invalid option")



        


