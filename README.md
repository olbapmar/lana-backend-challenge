#Lana Back-End challenge

Hey there! This is my implementation for the proposed challenge. Thank you for taking the time to check it out!

For this server I've chosen to use java, making use of the spring boot framework to speed up the process. 

### Project Structure
The structure of the code is as follows:

```
lanachallenge
    src/
        main/xyz/lana/lanachallenge
            controller // Used for the server http's endpoint and configuration
                util  // Useful stuff used by the controller
            exception // Defines a simple exceptions for cases where the requested id cannot be found
            model     // Self-explainatory: It defines the classes that represents the data we're dealing with
            service   // Internal services to deal with the data, here we'd add a DB implementation if necessary
                configuration // Used to specify configurations to apply to the session (Offers and service to be used)
                offer         // Types of offers we can handle
        test/java/xyz/lana/lanachallenge
            integtest // Integration tests
            unittests // Unit tests
    application.yml // Specifies configuration to apply to the server
    pom.xml         // build system and dependency management
```

### Design

Although this is a relatively simple project, I've taken several decisions that are worth mentioning.

- The offers are managed as classes and objects, making it easier and simplier to maintain and update (It's a matter of
  changing the objects filled in the offer List just adding or removing an object or its parameters). On top of it, the
  code looks much clearer this way.
  
- I used a service interface, so that changing from the in-memory basket handling service can easily be switched to use
a database or similar just by creating a class and changing the configuration. 
  
I used an approach which uses HTTP GET and query parameters to interact with the API. A more HTTP RESTful approach could
also have been used, and adapting the controller to it wouldn't require much effort.

### API documentation

As mentioned, the API uses HTTP GET endpoints, so the implementation looks as follows:

| Operation | Endpoint | Query Params | Return Type |
|---|---|---|---|
|Create basket | /newBasket | - | JSON
|Add product to basket | /addToBasket | basketId & productId | - |
|Remove basket | /deleteBasket | basketId | - |
|Getting price | /totalPrice | basketId | JSON |
|Getting basket | /getBasket | basketId | JSON |

The default port is `8080`. In case of an invalid id provided to the server (either basket or product) an error is sent
back to the client containing the damaging Id and a descriptive message.

### Build and deploy

#### Native approach
Being in this directory and assuming a valid java installation on the machine: 
1. `mvn clean verify` To run not only the build, but the integ tests
2. `cd target`
3. `java -jar lanachallenge-1.0.0.jar`

#### Docker approach
You can also use docker to both build and run the server. Simply use 

```
docker build -t lanachallenge:1.0 .
docker run -d -p 8080:8080 --name lana-container lanachallenge:1.0
```

And the you can test with the client, explained below.


### Client 

There is also a little client, created for convenience. It's developed in python so in order to run it you'll have to have
python installed locally:

`python client.py port`

The instructions to use it are displayed on runtime.

