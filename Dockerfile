FROM maven:3.8.1-jdk-11 AS MAVEN_BUILD

COPY ./ ./

RUN mvn clean verify

FROM openjdk:12-alpine

COPY --from=MAVEN_BUILD target/lanachallenge-1.0.0.jar /lana.jar

CMD ["java", "-jar", "lana.jar"]