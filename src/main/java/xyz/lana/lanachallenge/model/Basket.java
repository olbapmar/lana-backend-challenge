package xyz.lana.lanachallenge.model;

import lombok.Getter;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class Basket {

    @Getter private ConcurrentHashMap<Product, Integer> products;
    @Getter private final UUID basketId;

    public Basket() {
        this(UUID.randomUUID());
    }

    public Basket(UUID basketId) {
        this.basketId = basketId;
        this.products = new ConcurrentHashMap<>();
    }

    public void addProduct(Product product) {
        if (products.containsKey(product)) {
            products.put(product, products.get(product) + 1);
        } else {
            products.put(product, 1);
        }
    }

    public void removeProduct(Product product) {
        products.remove(product);
    }

    public void decreaseAmount(Product product) {
        if (products.containsKey(product)) {
            if (products.get(product) > 1) {
                products.put(product, products.get(product) - 1);
            } else {
                products.remove(product);
            }
        }
    }

    public int amountOf(Product product) {
        return products.containsKey(product) ? products.get(product) : 0;
    }

    @Override
    public String toString() {
        String retVal = String.format("Basket %s:\n", basketId.toString());

        for (Map.Entry<Product, Integer> entry : products.entrySet()) {
            retVal += String.format("\t%s(%d)", entry.getKey().getProductName(), entry.getValue());
        }

        return retVal;
    }
}
