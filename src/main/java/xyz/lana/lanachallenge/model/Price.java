package xyz.lana.lanachallenge.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Price {

    private static final RoundingMode DEFAULT_ROUNDING = RoundingMode.HALF_EVEN;

    @Getter @Setter private BigDecimal value;

    public Price(BigDecimal value) {
        this(value, DEFAULT_ROUNDING);
    }

    public Price(String value) {
        this(new BigDecimal(value), DEFAULT_ROUNDING);
    }

    public Price(BigDecimal value, RoundingMode rounding) {
        this.value = value.setScale(2, rounding);
    }

    @Override
    public String toString() {
        return getValue() + "€";
    }

    @Override
    public boolean equals(Object other){
        if (other != null && other.getClass() == this.getClass()) {
            if (value.equals(((Price)other).getValue())){
                return true;
            }
        }
        return false;
    }
}
