package xyz.lana.lanachallenge.model;

import lombok.Getter;
import xyz.lana.lanachallenge.exception.NoSuchElementException;

public enum Product {

    TSHIRT("1", "T-Shirt", "Lana T-Shirt", "20.00"),
    PEN("2", "Pen", "Lana pen", "5.00"),
    MUG("3", "Mug", "Lana mug", "7.50");

    @Getter private final String productId;
    @Getter private final String productName;
    @Getter private final String description;
    @Getter private final Price price;

    Product(String productId, String productName, String description, String price) {
        this.productId = productId;
        this.productName = productName;
        this.description = description;
        this.price = new Price(price);
    }

    // This is temporary as we have few products, we should use map instead in the amount of product increases
    public static Product fromId(String id) {
        for(Product product: values()) {
            if(product.productId.equals(id)){
                return product;
            }
        }
        throw new NoSuchElementException("No such product found", id);
    }

    @Override
    public String toString() {
        return String.format("%s (%s): %s", productName, description, price.toString());
    }
}
