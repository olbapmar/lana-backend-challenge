package xyz.lana.lanachallenge.exception;

import lombok.Getter;

public class NoSuchElementException extends RuntimeException {
    @Getter private String requestedId;

    public NoSuchElementException(String message, String requestedId) {
        super(message);
        this.requestedId = requestedId;
    }
}
