package xyz.lana.lanachallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LanachallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(LanachallengeApplication.class, args);
	}

}
