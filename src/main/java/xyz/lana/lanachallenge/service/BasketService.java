package xyz.lana.lanachallenge.service;

import xyz.lana.lanachallenge.model.Basket;
import xyz.lana.lanachallenge.model.Price;
import xyz.lana.lanachallenge.model.Product;

public interface BasketService {
    String createBasket();

    void addProductToBasket(String basketId, Product product);

    Price getBasketPrice(String basketId);

    Basket getBasket(String basketId);

    void deleteBasket(String basketId);
}
