package xyz.lana.lanachallenge.service;

import lombok.NonNull;
import xyz.lana.lanachallenge.exception.NoSuchElementException;
import xyz.lana.lanachallenge.model.Basket;
import xyz.lana.lanachallenge.model.Price;
import xyz.lana.lanachallenge.model.Product;
import xyz.lana.lanachallenge.service.offer.Offer;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class MemoryBasketService implements BasketService {

    private final List<Offer> activeOffers;
    private ConcurrentHashMap<UUID, Basket> basketsMap;

    public MemoryBasketService(@NonNull List<Offer> activeOffers) {
        this.activeOffers = activeOffers;
        this.basketsMap = new ConcurrentHashMap<>();
    }

    @Override
    public String createBasket() {
        Basket newBasket = new Basket();
        basketsMap.put(newBasket.getBasketId(), newBasket);
        return newBasket.getBasketId().toString();
    }

    @Override
    public void addProductToBasket(String basketId, Product product) {
        Basket basket = getBasket(basketId);

        if (basket != null) {
            basket.addProduct(product);
        }
    }

    @Override
    public Price getBasketPrice(String basketId) {
        Basket basket = getBasket(basketId);
        Price totalPrice = new Price(BigDecimal.ZERO);

        /**
         * We first calculate the total price without offers, and we then discount the offer from that price
         */
        if (basket != null) {
            for (Product product : basket.getProducts().keySet()) {
                BigDecimal priceOfThisProduct = product.getPrice().getValue().multiply(BigDecimal.valueOf(basket.getProducts().get(product)));
                totalPrice.setValue(totalPrice.getValue().add(priceOfThisProduct));
            }

            for (Offer offer : activeOffers) {
                Price priceToDiscount = offer.isOfferApplicable(basket);
                totalPrice.setValue(totalPrice.getValue().subtract(priceToDiscount.getValue()));
            }
        }

        return totalPrice;
    }

    @Override
    public Basket getBasket(String basketId) {
        Basket basket = basketsMap.get(UUID.fromString(basketId));

        if(basket != null) {
            return basket;
        } else {
            throw new NoSuchElementException("No such basket found", basketId);
        }
    }

    @Override
    public void deleteBasket(String basketId) {
        basketsMap.remove(UUID.fromString(basketId));
    }
}
