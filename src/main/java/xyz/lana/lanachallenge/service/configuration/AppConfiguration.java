package xyz.lana.lanachallenge.service.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import xyz.lana.lanachallenge.model.Product;
import xyz.lana.lanachallenge.service.BasketService;
import xyz.lana.lanachallenge.service.MemoryBasketService;
import xyz.lana.lanachallenge.service.offer.BulkPurchaseOffer;
import xyz.lana.lanachallenge.service.offer.BuyXGetOneOffer;
import xyz.lana.lanachallenge.service.offer.Offer;

import java.math.BigDecimal;
import java.util.List;

@Configuration
public class AppConfiguration {
    @Bean
    public List<Offer> getOffers() {
        return List.of(
                new BulkPurchaseOffer(Product.TSHIRT, 3, BigDecimal.valueOf(0.25)),
                new BuyXGetOneOffer(Product.PEN, 2)
        );
    }

    @Bean
    public BasketService getBasketService(List<Offer> offers) {
        return new MemoryBasketService(offers);
    }
}
