package xyz.lana.lanachallenge.service.offer;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import xyz.lana.lanachallenge.model.Basket;
import xyz.lana.lanachallenge.model.Price;
import xyz.lana.lanachallenge.model.Product;

import java.math.BigDecimal;

@AllArgsConstructor
public class BulkPurchaseOffer implements Offer {

    @NonNull private Product applicableProduct;
    private int minAmount = 0;
    @NonNull private BigDecimal discountRate;

    @Override
    public Price isOfferApplicable(Basket basket) {
        int amount = basket.amountOf(applicableProduct);

        if (amount >= minAmount) {
            return new Price(applicableProduct.getPrice().getValue()
                    .multiply(BigDecimal.valueOf(amount))
                    .multiply(discountRate));
        } else {
            return new Price(BigDecimal.ZERO);
        }
    }
}
