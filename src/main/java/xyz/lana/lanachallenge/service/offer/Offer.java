package xyz.lana.lanachallenge.service.offer;

import xyz.lana.lanachallenge.model.Basket;
import xyz.lana.lanachallenge.model.Price;

/*
This interface allow us to have generic offer (or even several similar offers) without having to use duplicated code,
on top of it, dealing with offers as objects gives us the possibility to simply modify the configuration to change the
offers characteristics. On top of that, it provides the potential to modify the list on runtime if needed (with an endpoint, etc)
 */
public interface Offer {
    Price isOfferApplicable(Basket basket);
}
