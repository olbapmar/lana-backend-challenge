package xyz.lana.lanachallenge.service.offer;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import xyz.lana.lanachallenge.model.Basket;
import xyz.lana.lanachallenge.model.Price;
import xyz.lana.lanachallenge.model.Product;

import java.math.BigDecimal;

@AllArgsConstructor
public class BuyXGetOneOffer implements Offer {

    @NonNull private Product applicableProduct;
    private int nOfProductsNeeded = 2;

    @Override
    public Price isOfferApplicable(Basket basket) {
        int amount = basket.amountOf(applicableProduct);

        if (amount >= nOfProductsNeeded) {
            return new Price(applicableProduct.getPrice().getValue()
                    .multiply(BigDecimal.valueOf(amount / nOfProductsNeeded)));
        } else {
            return new Price(BigDecimal.ZERO);
        }
    }
}
