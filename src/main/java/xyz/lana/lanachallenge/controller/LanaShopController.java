package xyz.lana.lanachallenge.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xyz.lana.lanachallenge.controller.util.NotFoundError;
import xyz.lana.lanachallenge.exception.NoSuchElementException;
import xyz.lana.lanachallenge.model.Basket;
import xyz.lana.lanachallenge.model.Price;
import xyz.lana.lanachallenge.model.Product;
import xyz.lana.lanachallenge.service.BasketService;

import java.util.Collections;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class LanaShopController {
    final BasketService basketService;

    @GetMapping(path = "/newBasket", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map> createNewBasket() {
        return new ResponseEntity<>(Collections.singletonMap("basketId", basketService.createBasket()), HttpStatus.CREATED);
    }

    @GetMapping(path="/addToBasket")
    public ResponseEntity<String> addProduct(@RequestParam(required = true, name = "basketId") String basketId,
                                             @RequestParam(required = true, name = "productId") String productId){
        Product product = Product.fromId(productId);
        basketService.addProductToBasket(basketId, product);

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @GetMapping(path="/deleteBasket")
    public ResponseEntity<String> deleteBasket(@RequestParam(required = true, name = "basketId") String basketId){
        basketService.deleteBasket(basketId);

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @GetMapping(path = "/totalPrice")
    public ResponseEntity<Map> totalPrice(@RequestParam(required = true, name = "basketId") String basketId){
        Price price = basketService.getBasketPrice(basketId);

        return new ResponseEntity<>(Collections.singletonMap("totalPrice", price.toString()), HttpStatus.OK);
    }

    @GetMapping(path = "/getBasket")
    public ResponseEntity<Basket> getBasket(@RequestParam(required = true, name = "basketId") String basketId) {
        return new ResponseEntity<>(basketService.getBasket(basketId), HttpStatus.OK);
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<NotFoundError> handleInvalidId(NoSuchElementException exception) {
        return new ResponseEntity<>(new NotFoundError(exception.getRequestedId(), exception.getMessage()), HttpStatus.NOT_FOUND);
    }
}
