package xyz.lana.lanachallenge.controller.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotFoundError {
    @Getter @NonNull private String requestedId;
    @Getter @NonNull private String message;
}

