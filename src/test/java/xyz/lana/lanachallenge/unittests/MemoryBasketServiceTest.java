package xyz.lana.lanachallenge.unittests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import xyz.lana.lanachallenge.exception.NoSuchElementException;
import xyz.lana.lanachallenge.model.Price;
import xyz.lana.lanachallenge.model.Product;
import xyz.lana.lanachallenge.service.BasketService;
import xyz.lana.lanachallenge.service.MemoryBasketService;
import xyz.lana.lanachallenge.service.offer.BulkPurchaseOffer;
import xyz.lana.lanachallenge.service.offer.BuyXGetOneOffer;
import xyz.lana.lanachallenge.service.offer.Offer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/*
This is the only unit test for now, the other clases are either too small for valid Unit Testing (model)
or directly used by the BasketService so closely that it makes no difference to perform them here or separately
 */

public class MemoryBasketServiceTest {
    private BasketService basketService;

    @Test
    void testBasicFunctionality(){
        basketService = new MemoryBasketService(new ArrayList<>());

        String basketId = basketService.createBasket();
        basketService.addProductToBasket(basketId, Product.MUG);
        basketService.addProductToBasket(basketId, Product.TSHIRT);
        basketService.addProductToBasket(basketId, Product.PEN);
        basketService.addProductToBasket(basketId, Product.PEN);

        Assertions.assertNotNull(basketService.getBasket(basketId));
        Assertions.assertEquals(3, basketService.getBasket(basketId).getProducts().size());
        Assertions.assertEquals(2, basketService.getBasket(basketId).amountOf(Product.PEN));

        basketService.deleteBasket(basketId);
        Assertions.assertThrows(NoSuchElementException.class, () ->basketService.getBasket(basketId));
    }

    @Test
    void testNoOffer() {
        basketService = new MemoryBasketService(new ArrayList<>());

        String basketId = basketService.createBasket();
        basketService.addProductToBasket(basketId, Product.MUG);
        basketService.addProductToBasket(basketId, Product.TSHIRT);
        basketService.addProductToBasket(basketId, Product.PEN);
        basketService.addProductToBasket(basketId, Product.PEN);

        Assertions.assertEquals(new Price(new BigDecimal("37.50")), basketService.getBasketPrice(basketId));
    }

    @Test
    void testWithOffers() {
        List<Offer> offers = List.of(
                new BuyXGetOneOffer(Product.PEN, 2),
                new BulkPurchaseOffer(Product.TSHIRT, 3, new BigDecimal("0.25"))
        );

        basketService = new MemoryBasketService(offers);

        String basketId = basketService.createBasket();
        basketService.addProductToBasket(basketId, Product.PEN);
        basketService.addProductToBasket(basketId, Product.PEN);
        basketService.addProductToBasket(basketId, Product.TSHIRT);
        basketService.addProductToBasket(basketId, Product.TSHIRT);
        basketService.addProductToBasket(basketId, Product.TSHIRT);

        Assertions.assertEquals(new Price(new BigDecimal("50.00")), basketService.getBasketPrice(basketId));
    }

    @Test
    void testWithEmptyBasket() {
        basketService = new MemoryBasketService(new ArrayList<>());
        String basketId = basketService.createBasket();

        Assertions.assertEquals(new Price(BigDecimal.ZERO), basketService.getBasketPrice(basketId));
    }
}
