package xyz.lana.lanachallenge.integtests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.util.UriComponentsBuilder;
import xyz.lana.lanachallenge.model.Basket;

import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class LanaChallengeIT {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private String getBasketURI(String basketId) {
        return UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/getBasket")
                .queryParam("basketId", basketId).toUriString();
    }

    private String totalPriceURU(String basketId) {
        return UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/totalPrice")
                .queryParam("basketId", basketId).toUriString();
    }

    private String addProductURI(String basketId, String productId) {
        return UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/addToBasket")
                .queryParam("basketId", basketId)
                .queryParam("productId", productId).toUriString();
    }

    private String deleteBasketURI(String basketId) {
        return UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/deleteBasket")
                .queryParam("basketId", basketId).toUriString();
    }

    private String newBasketURI() {
        return "http://localhost:" + port + "/newBasket";
    }

    @Test
    public void canCreateAndGetBasket() throws Exception {
        Map<String, String> newBasketResponse = this.restTemplate.getForObject(newBasketURI(), Map.class);
        assertThat(newBasketResponse).containsKey("basketId");
        assertThat(UUID.fromString(newBasketResponse.get("basketId"))).isNotNull();

        String basketId = newBasketResponse.get("basketId");

        Basket basketResponse = this.restTemplate.getForObject(getBasketURI(basketId),
                Basket.class);
        assertThat(basketResponse.getBasketId().toString()).isEqualTo(basketId);
    }

    @Test
    public void canAddToBasket() throws Exception {
        Map<String, String> newBasketResponse = this.restTemplate.getForObject(newBasketURI(), Map.class);

        String basketId = newBasketResponse.get("basketId");

        this.restTemplate.getForObject(addProductURI(basketId, "1"), String.class);
        this.restTemplate.getForObject(addProductURI(basketId, "2"), String.class);

        Basket basketResponse = this.restTemplate.getForObject(getBasketURI(basketId),
                Basket.class);
        assertThat(basketResponse.getProducts().size()).isEqualTo(2);
    }

    @Test
    public void canGetPrice() throws Exception {
        Map<String, String> newBasketResponse = this.restTemplate.getForObject(newBasketURI(), Map.class);
        String basketId = newBasketResponse.get("basketId");

        this.restTemplate.getForObject(addProductURI(basketId, "1"), String.class);
        this.restTemplate.getForObject(addProductURI(basketId, "1"), String.class);

        Map<String, String> priceResponse = this.restTemplate.getForObject(totalPriceURU(basketId),
                Map.class);
        assertThat(priceResponse.get("totalPrice")).isEqualTo("40.00€");

        // We add a new T-shirt to check that the offer is being applied
        this.restTemplate.getForObject(addProductURI(basketId, "1"), String.class);

        priceResponse = this.restTemplate.getForObject(totalPriceURU(basketId),
                Map.class);
        assertThat(priceResponse.get("totalPrice")).isEqualTo("45.00€");
    }

    @Test
    public void canDeleteBasket() throws Exception {
        Map<String, String> newBasketResponse = this.restTemplate.getForObject(newBasketURI(), Map.class);
        assertThat(newBasketResponse).containsKey("basketId");
        assertThat(UUID.fromString(newBasketResponse.get("basketId"))).isNotNull();

        String basketId = newBasketResponse.get("basketId");

        this.restTemplate.getForObject(deleteBasketURI(basketId), Map.class);

        String deleteResponse = this.restTemplate.getForObject(getBasketURI(basketId), String.class);
        assertThat(deleteResponse).contains("No such basket found");
    }
}
